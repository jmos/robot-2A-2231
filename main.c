/*
 * 	main.c
 *
 *  Created on: 26 févr. 2018
 *  Author: Joachim
 *	
 *	Fichier main du 2231 qui initialise le SPI et 
 *	attend qu'une interruption se passe pour traiter
 *	l'octet reçu
 *	
 *	P1.7|-- Data IN
 * 	P1.5|-- Horloge
 */

#include <msp430g2231.h>
#include <servo.h>

#define GAUCHE 	0xA1
#define DEVANT 	0xB1
#define DROITE 	0xC1

void init_SPI(void);

/*  ----------------------------------------------------
 *	Fonction : main
 *	----------------------------------------------------
 *	Fonction main qui arrête le watchdog, lance les initialisations
 *	du servo et SPI et active les interruptions
 *		entrées	: void
 *		sorties : void
 *	---------------------------------------------------- 
 */
void main(void)
{
    WDTCTL = WDTPW | WDTHOLD; /* stop watchdog timer */

    init_servo();
    init_SPI();
    servo_devant();

    __enable_interrupt();
}

/*  ----------------------------------------------------
 *	Fonction : init_SPI
 *	----------------------------------------------------
 *	Permet l'initialisation du SPI utilisant 2 fils :
 *	Data IN et Horloge
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_SPI(void)
{
    USICTL0 |= USIPE7 + USIPE6 + USIPE5 + USIOE; /* Port, SPI slave */
    USICTL1 |= USIIE | USICKPL | USICKPH; /* Counter interrupt, flag remains set */
    USICTL0 &= ~USISWRST; /* USI released for operation */
    USISRL = P1IN; /* init-load data */
    USICNT = 8; /* init-load counter : nb de bits a recevoir */
}

/*  ----------------------------------------------------
 *	Fonction : USI vector interruption
 *	----------------------------------------------------
 *	Fonction d'interruption déclenché à chaque caractère
 *	reçu en SPI
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
#pragma vector=USI_VECTOR
__interrupt void universal_serial_interface(void)
{
    switch (USISRL)
    {
    case GAUCHE:
        servo_gauche();
        break;
    case DEVANT:
        servo_devant();
        break;
    case DROITE:
        servo_droite();
        break;
    }
    USISRL = P1IN;	 /* re-load data */
    USICNT = 8; 	 /* re-load counter */

}
