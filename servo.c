/*
 * 	servo.c
 *
 *  Created on: 26 f�vr. 2018
 *  Author: Joachim
 *	
 *	Permet de commander le servo moteur
 *	
 *	P1.2|-- servo
 */

#include <msp430g2231.h>

/*  ----------------------------------------------------
 *	Fonction : init_servo
 *	----------------------------------------------------
 *	Permet l'initialisation du port P1.2 et de r�gler
 *	la fr�quence du PWM
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_servo(void) {
	/* choix multiplexage */
	P1DIR |= BIT2;
	P1SEL |= BIT2;

	/* r�glage PWM */
	TACTL |= (TASSEL_2 | ID_0 | MC_1);	/* SMCLK, Prediv /1, Mode UP */
	TACCTL1 |= OUTMOD_7;				/* Outmod : reset/set */
	TACCR0 = 20000; 					/* 20 ms */
}

/*	----------------------------------------------------
 *	Fonction : servo_devant
 *	----------------------------------------------------
 *	Permet de modifier le rapport cyclique du PWM
 *	afin de positionner le servo devant
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void servo_devant(void) {
	TACCR1 = 1500;
}

/*	----------------------------------------------------
 *	Fonction : servo_gauche
 *	----------------------------------------------------
 *	Permet de modifier le rapport cyclique du PWM
 *	afin de positionner le servo vers la gauche
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void servo_gauche(void) {
	TACCR1 = 2500;
}

/*	----------------------------------------------------
 *	Fonction : servo_droite
 *	----------------------------------------------------
 *	Permet de modifier le rapport cyclique du PWM
 *	afin de positionner le servo vers la droite
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void servo_droite(void) {
	TACCR1 = 500;
}

