/*
 * servo.h
 *
 *  Created on: 26 f�vr. 2018
 *      Author: Joachim
 */

#ifndef BUS_TP3_SERVO_H_
#define BUS_TP3_SERVO_H_


void init_servo(void);

void servo_devant(void);

void servo_droite(void);

void servo_gauche(void);



#endif /* BUS_TP3_SERVO_H_ */
